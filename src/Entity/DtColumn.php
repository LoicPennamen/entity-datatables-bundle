<?php

namespace LoicPennamen\EntityDataTablesBundle\Entity;

/**
 * Non-persisted entity designed for DataTable columns handling
 */
class DtColumn
{
//    public const SEARCH_STRING_BOUNDARIES_EXACT = 0;
//    public const SEARCH_STRING_BOUNDARIES_CONTAINS = 1;
//    public const SEARCH_STRING_BOUNDARIES_STARTS_WITH = 2;
//    public const SEARCH_STRING_BOUNDARIES_ENDS_WITH = 3;

//    public const SEARCH_STRING_DIVISION_FULL_STRING = 0;
//    public const SEARCH_STRING_DIVISION_EVERY_WORD = 1;
//    public const SEARCH_STRING_DIVISION_ANY_WORD = 2;

    // Camel-cased entity property name / column id
	private ?string $slug = null;
    // Full name of column
	private ?string $name = null;
    // Text visible in table's header
	private ?string $label = null;
    // Sorting slug. Must match entity property
	private ?string $sortingKey = null;
    // Custom template path
	private ?string $template = null;
    // Can be ordered by slug?
	private bool $sortable = true;
    // Convert label name to HTML
	private bool $htmlLabel = false;
    // Always return NULL values at the end of the table?
	private bool $sortNullValuesLast = true;

//     Search for string that match...
//	private int $searchStringBoundaries = self::SEARCH_STRING_BOUNDARIES_STARTS_WITH;
//     Search for every space-separated string instead of the full string?
//	private int $searchStringDivision = self::SEARCH_STRING_DIVISION_EVERY_WORD;

    public function setSlug($value): DtColumn
    {
		$this->slug = $value;
        return $this;
	}

	public function getSlug(): ?string
    {
		return $this->slug;
	}

	public function setName($value): DtColumn
    {
		$this->name = $value;
        return $this;
	}

	public function getName(): ?string
    {
		return $this->name;
	}

	public function setLabel($value): DtColumn
    {
		$this->label = $value;
        return $this;
	}

	public function getLabel(): ?string
    {
		return $this->label;
	}

	public function setSortable($value): DtColumn
    {
		$this->sortable = $value;
        return $this;
	}

	public function getSortable(): bool
    {
		return $this->sortable;
	}

	public function setSortingKey($value): DtColumn
    {
		$this->sortingKey = $value;
        return $this;
	}

	public function getSortingKey(): ?string
    {
		return $this->sortingKey;
	}

	public function setTemplate($value): DtColumn
    {
		$this->template = $value;
        return $this;
	}

	public function getTemplate(): ?string
    {
		return $this->template;
	}

	public function setHtmlLabel($value): DtColumn
    {
		$this->htmlLabel = $value;
        return $this;
	}

	public function getHtmlLabel(): bool
    {
		return $this->htmlLabel;
	}

	public function setSortNullValuesLast($value): DtColumn
    {
		$this->sortNullValuesLast = $value;
        return $this;
	}

	public function getSortNullValuesLast(): bool
    {
		return $this->sortNullValuesLast;
	}

//	public function setSearchStringBoundaries(int $value): DtColumn
//    {
//		$this->searchStringBoundaries = $value;
//        return $this;
//	}
//
//	public function getSearchStringBoundaries(): int
//    {
//		return $this->searchStringBoundaries;
//	}
//
//	public function setSearchStringDivision(int $value): DtColumn
//    {
//		$this->searchStringDivision = $value;
//        return $this;
//	}
//
//	public function getSearchStringDivision(): int
//    {
//		return $this->searchStringDivision;
//	}
}
