<?php

namespace LoicPennamen\EntityDataTablesBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EntityDatatablesExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return array(
            new TwigFunction('entity_datatables_uniqid', array($this, 'uniqid')),
        );
    }

    public function getName(): string
    {
        return 'LoicPennamenEntityDatatables';
    }

    public function uniqid(): string
    {
        return uniqid('edt-');
    }
}
