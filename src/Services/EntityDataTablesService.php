<?php
namespace LoicPennamen\EntityDataTablesBundle\Services;

use LoicPennamen\EntityDataTablesBundle\Entity\DtColumn;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

class EntityDataTablesService
{
    private Environment $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function getOptionsFromRequest(Request $request, array $tableColumns): array
    {
        $options = array_merge([
            'andWhere' => [],
            'andWherePredicates' => [],
            'join' => [],
            'leftJoin' => [],
            'search' => [],
            "draw" => "1",
            "columns" => [],
            "start" => "0",
            "length" => "25",
            "tableColumns" => $tableColumns,
        ], $options = $request->request->all());

        return $options;
    }

    public function getTableData($entities, array $columns, string $templateFolder = null, array $context = []): array
    {
        $rows = [];
        foreach ($entities as $entity) {
            $rows[] = $this->getRowHtml($entity, $columns, $templateFolder, $context);
        }
        return $rows;
    }

    public function getRowHtml($entity, array $columns, string $templateFolder = null, array $context = []): array
    {
        $row = [];

        foreach ($columns as $column) {
            if(!$column instanceof DtColumn) {
                throw new \Exception("DataTables columns must be of type LoicPennamen\EntityDataTablesBundle\Entity\DtColumn.");
            }
            $row[] = $this->getCellHtml($column, $entity, $templateFolder, $context);
        }

        return $row;
    }

    public function getCellHtml(DtColumn $column, $entity, string $templateFolder = null, array $context = []): string
    {
        // Entity name, by convention
        $entityFullName = get_class($entity);
        $entityName = substr($entityFullName, strrpos($entityFullName, '\\') +1);
        $entitySlug = lcfirst($entityName);
        $cellContext = array_merge($context, [$entitySlug => $entity]);

        // Possible template names
        $tplPaths = [
            $entitySlug . '/cell-' . $column->getSlug() . '.html.twig'
        ];
        // Template defined in column entity?
        if ($column->getTemplate()) {
            array_unshift($tplPaths, $column->getTemplate());
        }
        // Custom template folder on method call
        if ($templateFolder) {
            array_unshift($tplPaths, $templateFolder.'/cell-'.$column->getSlug().'.html.twig');
        }
        // Look for template file
        foreach($tplPaths as $templatePath) {
            if(true === $this->twig->getLoader()->exists($templatePath)){
                try {
                    $cellHtml = $this->twig->render(
                        $templatePath,
                        $cellContext
                    );
                    return $cellHtml;
                } catch (\Exception $exception) {
                    return $exception->getMessage();
                }
            }
        }

        // No template was found
        // Getting value by method name
        $methodName = 'get'.ucfirst($column->getSlug());
        if(method_exists($entity, $methodName)){
            try {
                // The method exists, and can return anything
                $value = $entity->{$methodName}();

                // Default date format:
                if ($value instanceof \DateTime) {
                    // Rely on Twig environment
                    $tpl = $this->twig->createTemplate('{{ date|format_datetime() }}');
                    return $tpl->render(['date' => $value]);
                }

                // Handle when ToString method exists
                if(is_object($value) && method_exists($value, '__toString')){
                    return $value->__toString();
                }
                if(is_object($value) && method_exists($value, 'toString')){
                    return $value->toString();
                }

                // Handle any: to string
                return (string)$value;

            } catch (\Exception $exception) {
                return '['.$column->getSlug().': '.$exception->getMessage().']';
            }
        }

        // Default value
        return '[' . $column->getSlug() . ']';
    }

}
