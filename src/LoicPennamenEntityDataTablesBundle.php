<?php
namespace LoicPennamen\EntityDataTablesBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class LoicPennamenEntityDataTablesBundle extends AbstractBundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    public function loadExtension(array $config, ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder): void
    {
        // Import config
        $containerConfigurator->import('../config/services.yaml');

        // you can also add or replace parameters and services
        // $containerConfigurator->parameters()
        //     ->set('acme_hello.phrase', $config['phrase'])
        // ;

    }
}
