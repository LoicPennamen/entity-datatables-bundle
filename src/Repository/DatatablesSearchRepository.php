<?php

namespace LoicPennamen\EntityDataTablesBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

class DatatablesSearchRepository extends ServiceEntityRepository
{
    public const SEARCH_STRING_BOUNDARIES_EXACT = 0;
    public const SEARCH_STRING_BOUNDARIES_CONTAINS = 1;
    public const SEARCH_STRING_BOUNDARIES_STARTS_WITH = 2;
    public const SEARCH_STRING_BOUNDARIES_ENDS_WITH = 3;

    public const SEARCH_STRING_DIVISION_FULL_STRING = 0;
    public const SEARCH_STRING_DIVISION_EVERY_WORD = 1;
    public const SEARCH_STRING_DIVISION_ANY_WORD = 2;

    public const QUERY_TYPE_COUNT = 0;
    public const QUERY_TYPE_SEARCH = 1;

    private string $entityAlias;
	private array $joins = [];
	private array $leftJoins = [];
	private array $searchFields = [];
	private array $andWhere = [];
	private array $orderBy = [];
    private bool $sortNullValuesLast = true;
    private int $searchStringBoundaries = self::SEARCH_STRING_BOUNDARIES_CONTAINS;
    private int $searchStringDivision = self::SEARCH_STRING_DIVISION_EVERY_WORD;

	public function __construct(ManagerRegistry $registry, string $entityClass = '')
	{
		parent::__construct($registry, $entityClass);

		if(!$this->entityAlias) {
            throw new \Exception("No entityAlias was defined for DatatablesSearchRepository");
        }
	}

	public function search(array $options)
	{
		$options['query_type'] = self::QUERY_TYPE_SEARCH;
		$qb = $this->getQueryBuilder($options);

		return new Paginator($qb);
	}

	// Count with filters
	public function countSearch(array $options = null)
	{
		$options['query_type'] = self::QUERY_TYPE_COUNT;
		$qb = $this->getQueryBuilder($options);
		$result = $qb->getQuery()->getOneOrNullResult();

		if(!$result)	return 0;
		else			return intval($result[1]);
	}

	// Count without filters but with mandatory parameters
	public function countSearchTotal(array $options)
	{
		$qb = $this->createQueryBuilder($this->entityAlias)
			->select("COUNT(DISTINCT($this->entityAlias.id))")
		;
		$qb = $this->addMandatoryParameters($qb, $options);
		$result = $qb->getQuery()->getOneOrNullResult();

		if(!$result)	return 0;
		else			return intval($result[1]);
	}

	// Create queryBuilder for multiple usages
	public function getQueryBuilder($options = null): QueryBuilder
	{
		$qb = $this->createQueryBuilder($this->entityAlias);

		// Selection subquery
		if(self::QUERY_TYPE_SEARCH === $options['query_type']){
            $qb->select($this->entityAlias);
        }
		if(self::QUERY_TYPE_COUNT === $options['query_type']){
            $qb->select("COUNT(DISTINCT($this->entityAlias.id))");
        }

		// Add joins
		foreach ($this->joins as $joinField => $joinAlias){
            $qb->join($joinField, $joinAlias);
        }
		foreach ($this->leftJoins as $joinField => $joinAlias){
            $qb->leftJoin($joinField, $joinAlias);
        }

		// Search
		if($options['search'] && $options['search']['value'] && count($this->searchFields) > 0) {
			$dqlSearchSnippets = [];
			$fullSearchString = trim($options['search']['value']);

            // Divisions: search the whole string?
            switch ($this->searchStringDivision) {
                case self::SEARCH_STRING_DIVISION_FULL_STRING:
                    $searchStrings = [$fullSearchString];
                    $snippetAggregator = null;
                    break;
                case self::SEARCH_STRING_DIVISION_EVERY_WORD:
                    $searchStrings = preg_split('/[\s;,]/', $fullSearchString);
                    $snippetAggregator = 'AND';
                    break;
                case self::SEARCH_STRING_DIVISION_ANY_WORD:
                    $searchStrings = preg_split('/[\s;,]/', $fullSearchString);
                    $snippetAggregator = 'OR';
                    break;
                default:
                    throw new \Exception("Invalid search string division option");
            }

            // Boundaries: Starting with, contains...
            for ($i = 0; $i < sizeof($searchStrings); $i++) {
                switch ($this->searchStringBoundaries) {
                    case self::SEARCH_STRING_BOUNDARIES_EXACT:
                        break;
                    case self::SEARCH_STRING_BOUNDARIES_CONTAINS:
                        $searchStrings[$i] = '%'.$searchStrings[$i].'%';
                        break;
                    case self::SEARCH_STRING_BOUNDARIES_STARTS_WITH:
                        $searchStrings[$i] = $searchStrings[$i].'%';
                        break;
                    case self::SEARCH_STRING_BOUNDARIES_ENDS_WITH:
                        $searchStrings[$i] = '%'.$searchStrings[$i];
                        break;
                    default:
                        throw new \Exception("Invalid search string boudaries option");
                }
            }

			// Search in each "searchable" entity field
			foreach($this->searchFields as $field) {
                $snippet = '';
                // Search for each substring
                for ($i = 0; $i < sizeof($searchStrings); $i++) {
                    $snippet .= ($snippet ? $snippetAggregator : '') . " $field LIKE :searchValue_$i ";
                }
				$dqlSearchSnippets[] = $snippet;
            }

            // This method applies filtering to each entity property "hermetically".
            // Cross-property filtering would require a refactoring and an additional config option
			$qb->andWhere("(".implode(' OR ', $dqlSearchSnippets).")");
            for ($i = 0; $i < sizeof($searchStrings); $i++) {
                $qb->setParameter("searchValue_$i", $searchStrings[$i]);
            }
		}

		// Add filters
		foreach ($this->andWhere as $andWhere) {
            $qb->andWhere($andWhere);
        }

		// Specific mandatory parameters
		$qb = $this->addMandatoryParameters($qb, $options);

		// Sorting:
		if(self::QUERY_TYPE_SEARCH === $options['query_type']
			&& isset($options['order'])
			&& isset($options['tableColumns'])
        ){
			/** @var DtColumn $dtColumn */
			foreach($options['order'] as $order){
				$sortingColumnIndex = intval($order['column']);
				$dtColumn = $options['tableColumns'][$sortingColumnIndex];

				if(true === $dtColumn->getSortable()){

					// Vars from options
					$sortingKey = $dtColumn->getSortingKey() ?: $this->entityAlias.'.'.$dtColumn->getSlug();
					$sortingDir = $order['dir'];

					// Nulls as first when sorting ASC
					if(false === $dtColumn->getSortNullValuesLast()) {
						$qb->addOrderBy($sortingKey, $sortingDir);
                    }

					// Nulls always last:
					else{
						// One can't "order by case" in DQL. To sort NULL values last,
						// we shall use an alias with SELECT HIDDEN, and sort on this subselection
						$sortingAlias = "_sorting_alias_$sortingColumnIndex";
						$qb->addSelect(" CASE WHEN $sortingKey IS NULL THEN 1 ELSE 0 END AS HIDDEN $sortingAlias ");
						$qb->addOrderBy($sortingAlias);
						$qb->addOrderBy($sortingKey, $sortingDir);
					}
				}
			}
		}

		// Add default sortings
		foreach($this->orderBy as $sortArr) {
            $qb->addOrderBy($sortArr[0], $sortArr[1]);
        }

		// Add pagination parameters
		if(self::QUERY_TYPE_SEARCH === $options['query_type']){
			$qb->setFirstResult($options['start']);
            if($options['length'] > 0){
                $qb->setMaxResults($options['length']);
            }
		}

		return $qb;
	}

	private function addMandatoryParameters(QueryBuilder $qb, array $options)
	{
		// Specific mandatory parameters
		foreach($options['andWhere'] as $field => $value){
			$valueAlias = str_replace('.', '_', $field);
			$qb->andWhere("$this->entityAlias.$field = :$valueAlias")
				->setParameter($valueAlias, $value);
		}

		return $qb;
	}

	public function setEntityAlias($entityAlias): self
	{
		$this->entityAlias = $entityAlias;
        return $this;
	}

	public function addJoin($joinField, $joinAlias): self
	{
		$this->joins[$joinField] = $joinAlias;
        return $this;
	}

	public function addLeftJoin($joinField, $joinAlias): self
	{
		$this->leftJoins[$joinField] = $joinAlias;
        return $this;
	}

	public function addSearchField($searchField): self
	{
		$this->searchFields[] = $searchField;
        return $this;
	}

	public function addAndWhere($andWhere): self
	{
		$this->andWhere[] = $andWhere;
        return $this;
	}

	public function addOrderBy($orderBy, $dir = 'ASC'): self
	{
		$this->orderBy[] = [$orderBy, $dir];
        return $this;
	}

	public function setSortNullValuesLast($value): self
    {
		$this->sortNullValuesLast = $value;
        return $this;
	}

	public function setSearchStringBoundaries(int $value): self
    {
		$this->searchStringBoundaries = $value;
        return $this;
	}

	public function setSearchStringDivision(int $value): self
    {
		$this->searchStringDivision = $value;
        return $this;
	}
}
